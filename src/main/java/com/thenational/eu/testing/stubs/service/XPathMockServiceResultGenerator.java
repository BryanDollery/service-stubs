package com.thenational.eu.testing.stubs.service;

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.MustacheFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.xml.sax.InputSource;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import static com.thenational.eu.testing.stubs.service.Utils.loadFile;
import static org.apache.commons.lang3.StringUtils.isEmpty;

@Component
public class XPathMockServiceResultGenerator implements MockServiceResultGenerator {
    private static final Logger LOGGER = LoggerFactory.getLogger(XPathMockServiceResultGenerator.class);

    private static final XPath xpath = XPathFactory.newInstance().newXPath();

    private MustacheFactory mf = new DefaultMustacheFactory();

    @Autowired
    private Utils utils;

    public String generate(String message, Context ctx) {
        XPathMockService annotation = ctx.getXpathMockService();

        if (!isEmpty(annotation.xpath()))
            return handleXpathDefinesFilename(message, ctx);
        else if (annotation.namedXpaths() != null && annotation.namedXpaths().length > 0)
            return handleXpathAndTemplate(message, annotation, ctx);

        throw new AnnotationMisconfiguredException("Expected one of: xpath, namedXpaths, or xpathAndFiles, but none were found");
    }

    private String handleXpathDefinesFilename(String message, Context ctx) {
        XPathMockService ann = ctx.getXpathMockService();

        try {
            String filename = xpath.evaluate(ann.xpath(), new InputSource(new StringReader(message)));

            if (isEmpty(filename)) {
                LOGGER.debug("Didn't find a filename value for xpath: " + ann.xpath());

                if (isEmpty(ann.file())) {
                    LOGGER.debug("Didn't find a default file for when this xpath fails: " + ann.xpath());
                    throw new AnnotationMisconfiguredException("Didn't find a default file for when this xpath fails: " + ann.file());
                }
            }

            String file = ann.path() + ann.filePrefix() + (!isEmpty(filename) ? filename : ann.file()) + ann.filePostfix() + ann.fileExtension();

            return ctx.resolve(file);
        } catch (XPathExpressionException e) {
            throw new AnnotationMisconfiguredException("Invalid xpath expression: " + ann.xpath(), e);
        }
    }

    private String handleXpathAndTemplate(String message, XPathMockService annotation, Context ctx) {
        String templateFile = loadFile(ctx.getResponseKey());
        Map<String, String> scope = constructScope(message, annotation);

        try {
            return mf.compile(templateFile).execute(new StringWriter(), scope).toString();
        } catch (Exception e) {
            StringBuilder sb = new StringBuilder("{");

            for (Map.Entry<String, String> entry : xpath2var.entrySet()) {
                if (sb.length() > 1) sb.append(", ");
                sb.append(entry.getKey()).append(" : ").append(entry.getValue());
            }

            sb.append("}");

            throw new AnnotationMisconfiguredException("Unable to process template 'classpath:" + ctx.getResponseKey() + "' with values " + sb.toString());
        }
    }

    private Map<String, String> constructScope(String message, XPathMockService annotation) {
        Map<String, String> scope = new HashMap<>();
        String xpathKey = "<unknown>";

        try {
            Map<String, String> xpath2var = new HashMap<>();

            utils.mapNameValuePairs(xpath2var, annotation.namedXpaths());

            for (Map.Entry<String, String> entry : xpath2var.entrySet()) {
                xpathKey = entry.getKey();

                String value = xpath.evaluate(xpathKey, new InputSource(new StringReader(message)));

                if (!isEmpty(value))
                    scope.put(entry.getValue(), value);
            }
            return xpath2var;
        } catch (Exception e) {
            throw new UnhelpfulXpathException("Couldn't make use of xpath expression: " + xpathKey, e);
        }
    }

}
