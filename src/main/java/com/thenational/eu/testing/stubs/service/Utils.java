package com.thenational.eu.testing.stubs.service;

import com.google.common.io.Resources;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.google.common.io.Resources.getResource;
import static java.nio.charset.Charset.defaultCharset;
import static org.springframework.util.ReflectionUtils.findMethod;
import static org.springframework.util.ReflectionUtils.invokeMethod;

@Component
public class Utils {
    private static final Logger LOGGER = LoggerFactory.getLogger(Utils.class);
    private static Pattern pattern = Pattern.compile("xmlns:.*?=(\".*?\")");
    private static Matcher matcher = pattern.matcher("");
    @Autowired
    private ApplicationContext applicationContext;
    @Autowired
    private XPathMockServiceResultGenerator xPathMockServiceResultGenerator;
    @Autowired
    private MonotonicMockServiceResultGenerator monotonicMockServiceResultGenerator;
    @Autowired
    private SequentialMockServiceResultGenerator sequentialMockServiceResultGenerator;
    @Autowired
    private RegexMockServiceResultGenerator regexMockServiceResultGenerator;
    @Autowired
    private ResponseContentResolver defaultResponseContextResolver;

    public static String getName(Context ctx) {
        return getName(ctx.getAnnotation());
    }

    public static String getName(Annotation annotation) {
        return getFromAnnotation(annotation, "name");
    }

    public static String getSchema(Annotation annotation) {
        return getFromAnnotation(annotation, "schema");
    }

    public static String getSchema(Context ctx) {
        return getSchema(ctx.getAnnotation());
    }

    public static String getFile(Context ctx) {
        return getFromAnnotation(ctx.getAnnotation(), "file");
    }

    public static String getExtension(Context ctx) {
        return getFromAnnotation(ctx.getAnnotation(), "fileExtension");
    }

    public static String getPrefix(Context ctx) {
        return getFromAnnotation(ctx.getAnnotation(), "filePrefix");
    }

    public static String getPostfix(Context ctx) {
        return getFromAnnotation(ctx.getAnnotation(), "filePostfix");
    }

    public static String getPath(Context ctx) {
        return getFromAnnotation(ctx.getAnnotation(), "path");
    }

    public static String getFromAnnotation(Annotation annotation, String field) {
        Method nameMethod = findMethod(annotation.getClass(), field);
        return (String) invokeMethod(nameMethod, annotation);
    }

    public static String loadFile(final String resourceName) {
        try {
            return Resources.toString(getResource(resourceName), defaultCharset());
        } catch (IOException e) {
            LOGGER.error("Failed to load classpath resource: {}  -- expect null pointer exceptions from this point on", resourceName);
            throw new IllegalArgumentException("Not a valid resource name", e);
        }
    }

    public Context findContext(String forSchema) {
        try {
            for (Object bean : applicationContext.getBeansWithAnnotation(MockService.class).values()) {
                final Annotation annotation = findMockServiceAnnotation(bean);
                String annotatedSchema = getSchema(annotation);

                if (forSchema.equals(annotatedSchema))
                    return new Context(annotation, bean, this);
            }
        } catch (Exception e) {
            // we want to throw the NoHandlerFound exception, but we're about to do that anyway...
        }

        throw new NoHandlerFound(forSchema);
    }

    public ResponseContentResolver findResponseContentResolver(Object service) {
        ResponseContentResolver responseContentResolverOverride = findResponseContentResolverOverride(service);

        return responseContentResolverOverride != null ? responseContentResolverOverride : defaultResponseContextResolver;
    }

    private ResponseContentResolver findResponseContentResolverOverride(Object service) {
        Method[] declaredMethods = service.getClass().getDeclaredMethods();


        try {
            for (Method method : declaredMethods) {
                Class<?> returnType = method.getReturnType();
                if (returnType.equals(ResponseContentResolver.class))
                    return (ResponseContentResolver) method.invoke(service);
            }
        } catch (Exception e) {
            LOGGER.info("Failed to use custom response content resolver. Using default (file resolver) instead");
        }

        return null;
    }

    private Annotation findMockServiceAnnotation(Object bean) {
        for (Annotation annotation : bean.getClass().getAnnotations())  // getAnnotation(MockService.class) and it's related methods doesn't work here so we have to search
            if (annotation.annotationType().getAnnotation(MockService.class) != null)
                return annotation;

        throw new AnnotationMisconfiguredException("Can't find mock service annotation on bean -- whoa -- that's not even possible here");
    }

    public Map<String, String> mapNameValuePairs(NameValuePairs[] values, int count) {
        Map<String, String> ctx = new HashMap<>(values.length);
        NameValuePairs current = values[count];
        mapNameValuePairs(ctx, current.value());
        return ctx;
    }

    public void mapNameValuePairs(Map<String, String> ctx, NameValuePair[] nvp) {
        for (NameValuePair value : nvp)
            ctx.put(value.name(), value.value());
    }

    public MockServiceResultGenerator findGenerator(Context ctx) {
        if (ctx.isXpath()) {
            return xPathMockServiceResultGenerator;
        } else if (ctx.isMonotonic()) {
            return monotonicMockServiceResultGenerator;
        } else if (ctx.isRegex()) {
            return regexMockServiceResultGenerator;
        } else if (ctx.isSequential()) {
            return sequentialMockServiceResultGenerator;
        }

        throw new AnnotationMisconfiguredException("Can't find associated generator -- probably a programmer error");
    }

    // example: <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><soap:Body><NagMsg xmlns:ns2="http://schemas.eu.nabgroup.com/GCS_INQ_ACCTDETAILS_BS_V02/ACCTDETSINQRS/" xmlns="http://schemas.eu.nabgroup.com/GCS_INQ_ACCTDETAILS_BS_V02/ACCTDETSINQRQ/"><GblMsgHdr><HdrInfo><HdrVersNbrId>001</HdrVersNbrId></HdrInfo><MsgResp><RespCode>0</RespCode><RsnCode>0</RsnCode><SnsDataDesc>0</SnsDataDesc></MsgResp><MsgIdn><SrcRoutInfo><Chnl>BPI</Chnl></SrcRoutInfo></MsgIdn><MsgAttrib><SrvcId>GCS_INQ_ACCTDETAILS_BS_V02</SrvcId><MsgType>Request</MsgType><RespReqdInd>Y</RespReqdInd><DtTm><LocalDtTm>2016-02-22T16:33:20.313Z</LocalDtTm></DtTm></MsgAttrib><PartyAttrib><OrigId>000000</OrigId><CnsmrId>ETCB0001</CnsmrId><CnsmrPrimFI>CB</CnsmrPrimFI></PartyAttrib></GblMsgHdr><SIGNONMSGSRQV1><SONRQ><CHANNELID>BPI</CHANNELID></SONRQ></SIGNONMSGSRQV1><PAYLOAD><PAYLOADTYPE>ACCTDETSINQRQ</PAYLOADTYPE><ACCTDETAILVIEW><BANKACCTINFO><BANKACCTINFOGRP TYPE="FROM"><BANKID>82</BANKID><BRANCHID>4707</BRANCHID><ACCTID>30089369</ACCTID><ACCTTYPE>IMP</ACCTTYPE></BANKACCTINFOGRP></BANKACCTINFO></ACCTDETAILVIEW></PAYLOAD></NagMsg></soap:Body></soap:Envelope>
    public String findSchema(String in) {
        matcher.reset(in);

        if (matcher.find()) {
            String schema = matcher.group(1);
            return schema.substring(1, schema.length() - 1);
        }

        return "";
    }

}
