package com.thenational.eu.testing.stubs.service;

public class NonSoapRequestException extends RuntimeException {
    public NonSoapRequestException(String message) {
        super(message);
    }
}
