package com.thenational.eu.testing.stubs.service;

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.MustacheFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.StringWriter;
import java.util.Map;

import static org.apache.commons.lang3.ArrayUtils.isEmpty;

@Component
public class SequentialMockServiceResultGenerator implements MockServiceResultGenerator {
    private int count = 0;
    private MustacheFactory mf = new DefaultMustacheFactory();

    @Autowired
    private Utils utils;

    public String generate(String message, Context ctx) {
        SequentialMockService annotation = ctx.getSequentialMockService();

        if (isEmpty(annotation.files()))
            throw new AnnotationMisconfiguredException("Expected files, but found none");

        if (!isEmpty(annotation.values()))
            return handleTemplates(annotation);

        return handleFiles(annotation, ctx);
    }

    private String handleTemplates(SequentialMockService annotation) {
        String[] files = annotation.files();
        String file = files[count++];
        String filename = annotation.path() + annotation.filePrefix() + file + annotation.filePostfix() + annotation.fileExtension();
        String templateFile = utils.loadFile(filename);

        Map<String, String> ctx = utils.mapNameValuePairs(annotation.values(), count++);

        try {
            return mf.compile(templateFile).execute(new StringWriter(), ctx).toString();
        } catch (Exception e) {
            StringBuilder sb = new StringBuilder("{");

            for (Map.Entry<String, String> entry : ctx.entrySet()) {
                if (sb.length() > 1) sb.append(", ");
                sb.append(entry.getKey()).append(" : ").append(entry.getValue());
            }

            sb.append("}");

            throw new AnnotationMisconfiguredException("Unable to process template 'classpath:" + filename + "' with values " + sb.toString());
        }
    }


    private String handleFiles(SequentialMockService annotation, Context ctx) {
        String[] files = annotation.files();
        String file = files[count++];
        String filename = annotation.path() + annotation.filePrefix() + file + annotation.filePostfix() + annotation.fileExtension();
        return ctx.resolve(filename);
    }
}
