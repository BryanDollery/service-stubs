package com.thenational.eu.testing.stubs.service;

public interface MockServiceResultGenerator {
    String generate(String message, Context ctx);
}
