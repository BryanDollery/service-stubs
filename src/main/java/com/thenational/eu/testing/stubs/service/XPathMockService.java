package com.thenational.eu.testing.stubs.service;

import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Documented
@Inherited
@Retention(RUNTIME)
@Target(TYPE)
@MockService
/**
 * Use xpath to decide which response to send. The file params are used like this:
 * <code>String file = service.path() + service.filePrefix() + filename + service.filePostfix() + service.fileExtension();</code><br/>
 * Pay attention -- I don't add any slashes or spaces or dots -- I just concatenate the bits you give me. This is so that you can get
 * away with only providing the filename if necessary
 */
public @interface XPathMockService {
    /**
     * A name for the service, defaults to the class name with a lower-case first letter
     */
    String name() default "";

    /**
     * Schema to respond to
     */
    String schema();

    /**
     * Use a single xpath is to identify the response filename
     */
    String xpath() default "";

    /**
     * Used for templating. One xpath must be named 'file' (unless the default is used) -- it identifies the template to use. The rest are used to retrieve values
     * that will be replaced in the template by name. Useful for pulling out account-uuid/scan/customerId and putting them in the response
     */
    NameValuePair[] namedXpaths() default {};

    /**
     * Default file to use if the xpath doesn't match anything
     */
    String file() default "";

    /**
     * Resource path, not filesystem path
     */
    String path() default "";

    String filePrefix() default "";

    String filePostfix() default "";

    String fileExtension() default ".xml";
}
