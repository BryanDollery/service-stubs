package com.thenational.eu.testing.stubs.service;

public interface ResponseContentResolver {
    String resolve(String key);
}
