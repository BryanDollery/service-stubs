package com.thenational.eu.testing.stubs.service;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

import static com.google.common.base.CaseFormat.LOWER_CAMEL;
import static com.google.common.base.CaseFormat.UPPER_CAMEL;
import static org.springframework.util.ObjectUtils.isEmpty;

public class Context {
    private Annotation annotation;
    private Object bean;
    private Utils utils;
    private List<RequestResponse> requestResponses = new ArrayList<>();

    public Context(Annotation annotation, Object bean, Utils utils) {
        this.annotation = annotation;
        this.bean = bean;
        this.utils = utils;
    }

    public Annotation getAnnotation() {
        return annotation;
    }

    public Object getBean() {
        return bean;
    }

    public XPathMockService getXpathMockService() {
        return (XPathMockService) annotation;
    }

    public MonotonicMockService getMonotonicMockService() {
        return (MonotonicMockService) annotation;
    }

    public RegexMockService getRegexMockService() {
        return (RegexMockService) annotation;
    }

    public SequentialMockService getSequentialMockService() {
        return (SequentialMockService) annotation;
    }

    public boolean isXpath() {
        return annotation instanceof XPathMockService;
    }

    public boolean isMonotonic() {
        return annotation instanceof MonotonicMockService;
    }

    public boolean isRegex() {
        return annotation instanceof RegexMockService;
    }

    public boolean isSequential() {
        return annotation instanceof SequentialMockService;
    }

    public String getName() {
        String name = Utils.getName(annotation);

        if (isEmpty(name))
            return UPPER_CAMEL.to(LOWER_CAMEL, bean.getClass().getSimpleName());

        return name;
    }

    public String getSchema() {
        return Utils.getSchema(annotation);
    }

    public void addRequestAndResponse(String request, String response) {
        requestResponses.add(new RequestResponse(request, response));
    }

    public String getRequest(int i) {
        return requestResponses.get(i).request;
    }

    public String getResponse(int i) {
        return requestResponses.get(i).response;
    }

    public String getResponseKey() {
        return Utils.getPath(this) + Utils.getPrefix(this) + Utils.getFile(this) + Utils.getPostfix(this) + Utils.getExtension(this);
    }

    public String resolve(String responseKey) {
        ResponseContentResolver responseContentResolver = utils.findResponseContentResolver(bean);
        return responseContentResolver.resolve(responseKey);
    }

    private static class RequestResponse {
        String request, response;

        public RequestResponse(String request, String response) {
            this.request = request;
            this.response = response;
        }
    }
}
