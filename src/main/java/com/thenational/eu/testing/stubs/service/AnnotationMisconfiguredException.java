package com.thenational.eu.testing.stubs.service;

public class AnnotationMisconfiguredException extends RuntimeException {
    public AnnotationMisconfiguredException(String message) {
        super(message);
    }

    public AnnotationMisconfiguredException(String message, Throwable cause) {
        super(message, cause);
    }
}
