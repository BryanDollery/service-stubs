package com.thenational.eu.testing.stubs.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class StubService {
    private static final Logger LOGGER = LoggerFactory.getLogger(StubService.class);

    private static final String NAG_MSG_END = "</NagMsg>";
    private static final String NAG_MSG_START = "<NagMsg";

    @Autowired
    private Utils utils;

    private Map<String, Context> schema2ctx = new HashMap<>();


    @JmsListener(destination = "REQUEST", containerFactory = "jmsListenerContainerFactory")
    @SendTo("RESPONSE")
    public String esbReceiver(final String request) throws Throwable {
        LOGGER.debug("Received message: {}", request);
        validate(request);
        final String nagMsg = findNagMessage(request);
        final String schema = utils.findSchema(nagMsg);
        LOGGER.debug("Found Schema: {}", schema);
        final Context ctx = findContext(schema);
        LOGGER.debug("Found result generator: {} annotated with: @{}", ctx.getBean().getClass().getSimpleName(), ctx.getAnnotation().getClass().getSimpleName());
        final String response = utils.findGenerator(ctx).generate(request, ctx);
        LOGGER.debug("Sending response: {}", response);
        ctx.addRequestAndResponse(request, response);
        return response;
    }

    private void validate(String request) {
        if (!request.startsWith("<soap:Envelope"))
            throw new NonSoapRequestException("Received: " + request);
    }

    private String findNagMessage(String request) {
        final int nagMsgStart = request.indexOf(NAG_MSG_START);
        final int nagMsgEnd = request.indexOf(NAG_MSG_END);
        return request.substring(nagMsgStart, nagMsgEnd - NAG_MSG_END.length());
    }

    public Context findContext(String forSchema) {
        if (hasCtx(forSchema)) return getCtx(forSchema);
        Context ctx = utils.findContext(forSchema);
        schema2ctx.put(forSchema, ctx);
        return ctx;
    }

    public Context getCtx(String forSchema) {
        return schema2ctx.get(forSchema);
    }

    public boolean hasCtx(String forSchema) {
        return schema2ctx.containsKey(forSchema);
    }
}
