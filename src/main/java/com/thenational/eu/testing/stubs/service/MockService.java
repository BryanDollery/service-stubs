package com.thenational.eu.testing.stubs.service;

import org.springframework.stereotype.Component;

import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Documented
@Inherited
@Retention(RUNTIME)
@Target(TYPE)
@Component
public @interface MockService {
//    String schema();
//
//    String type();
//
//    String name();
//
//    String regex();
//
//
//    NameValuePairs[] values() default {};
//
//    /**
//     * Use a single xpath to identify the response filename
//     */
//    String xpath() default "";
//
//    /**
//     * Used for templating. One xpath must be named 'file' (unless the default is used) -- it identifies the template to use. The rest are used to retrieve values
//     * that will be replaced in the template by name. Useful for pulling out account-uuid/scan/customerId and putting them in the response
//     */
//    NameValuePair[] namedXpaths() default {};
//
//
//    /**
//     * Default file to use if the regex doesn't match
//     */
//    String file() default "";
//
//
//    /**
//     * Resource path, not filesystem path
//     */
//    String path() default "";
//
//    String filePrefix() default "";
//
//    String filePostfix() default "";
//
//    String fileExtension() default "";
}
