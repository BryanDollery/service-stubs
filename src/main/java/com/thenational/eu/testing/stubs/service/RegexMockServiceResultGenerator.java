package com.thenational.eu.testing.stubs.service;

import org.apache.commons.lang3.NotImplementedException;
import org.springframework.stereotype.Component;

@Component
public class RegexMockServiceResultGenerator implements MockServiceResultGenerator {
    public String generate(String message, Context handler) {
        throw new NotImplementedException("This hasn't been implemented yet becuase of lack of interest. If you'd really like to use it feel free to implement it");
    }
}
