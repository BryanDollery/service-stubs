package com.thenational.eu.testing.stubs.service;

import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Documented
@Inherited
@Retention(RUNTIME)
@Target(TYPE)
@MockService
/**
 * Send a series of responses, no matter what request was received
 */
public @interface SequentialMockService {
    /**
     * A name for the service, defaults to the class name with a lower-case first letter
     */
    String name() default "";

    /**
     * Schema to respond to
     */
    String schema();

    /**
     * Names the template to use in conjunction with 'values'.
     */
    String file() default "";

    /**
     * Used to provide values to templates returned sequentially -- so the first set of values are used in
     * the first response, the second set in the second response, etc.
     */
    NameValuePairs[] values() default {};

    /**
     * List of files to send in order -- all are prefixed and postfixed and extended as specified by the other attributes
     */
    String[] files() default "";


    /**
     * Resource path, not filesystem path
     */
    String path() default "";

    String filePrefix() default "";

    String filePostfix() default "";

    String fileExtension() default "";
}
