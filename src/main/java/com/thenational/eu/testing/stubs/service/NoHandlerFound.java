package com.thenational.eu.testing.stubs.service;

public class NoHandlerFound extends RuntimeException {
    public NoHandlerFound(String schema) {
        super("No handler found for schema: " + schema);
    }
}
