package com.thenational.eu.testing.stubs.service;

import org.springframework.stereotype.Component;

@Component
public class FileResponseContentResolver implements ResponseContentResolver {

    public String resolve(String key) {
        return Utils.loadFile(key);
    }

}
