package com.thenational.eu.testing.stubs.service;

import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Documented
@Inherited
@Retention(RUNTIME)
@Target(TYPE)
@MockService
/**
 * Use a regex to decide which response to send. If you use a single capture group then we will use it as part of the filename to respond with, but we
 * also support templating through named capture groups. In which case we're looking for a capture group named 'file' to identify the template to use
 * and we substitute placeholders that match the names of the other capture groups.
 */
public @interface RegexMockService {
    /**
     * A name for the service, defaults to the class name with a lower-case first letter
     */
    String name() default "";

    /**
     * Schema to respond to
     */
    String schema();

    String regex();

    /**
     * Default file to use if the regex doesn't match
     */
    String file() default "";

    /**
     * Resource path, not filesystem path
     */
    String path() default "";

    String filePrefix() default "";

    String filePostfix() default "";

    String fileExtension() default "";
}
