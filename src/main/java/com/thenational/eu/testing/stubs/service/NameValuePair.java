package com.thenational.eu.testing.stubs.service;

import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Documented
@Inherited
@Retention(RUNTIME)
@Target(TYPE)
/**
 * Don't use directly -- it's used in various MockService annotations
 */
public @interface NameValuePair {
    String name();

    String value();
}
