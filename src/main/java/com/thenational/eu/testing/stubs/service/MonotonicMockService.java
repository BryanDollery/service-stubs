package com.thenational.eu.testing.stubs.service;

import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Documented
@Inherited
@Retention(RUNTIME)
@Target(TYPE)
@MockService
/**
 * Sends the same response no matter what the request
 */
public @interface MonotonicMockService {
    /**
     * A name for the service, defaults to the class name with a lower-case first letter
     */
    String name() default "";

    /**
     * Schema to respond to
     */
    String schema();

    /**
     * Filename containing response
     */
    String file();

    /**
     * Resource path, not filesystem path
     */
    String path() default "";

    String filePrefix() default "";

    String filePostfix() default "";

    String fileExtension() default "";
}
