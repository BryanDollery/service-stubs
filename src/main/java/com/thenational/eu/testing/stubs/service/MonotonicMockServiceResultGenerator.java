package com.thenational.eu.testing.stubs.service;

import org.springframework.stereotype.Component;

@Component
public class MonotonicMockServiceResultGenerator implements MockServiceResultGenerator {
    public String generate(String message, Context ctx) {
        try {
            return ctx.resolve(ctx.getResponseKey());
        } catch (Exception e) {
            throw new AnnotationMisconfiguredException("Expected file to point to a classpath file to return, but I didn't get one... " + ctx.getResponseKey());
        }
    }
}
