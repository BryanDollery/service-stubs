package com.thenational.eu.testing.stubs.service;

public class UnhelpfulXpathException extends RuntimeException {
    public UnhelpfulXpathException(String message) {
        super(message);
    }

    public UnhelpfulXpathException(String message, Throwable cause) {
        super(message, cause);
    }
}
