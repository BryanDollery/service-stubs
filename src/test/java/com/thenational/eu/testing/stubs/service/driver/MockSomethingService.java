package com.thenational.eu.testing.stubs.service.driver;

import com.thenational.eu.testing.stubs.service.NameValuePair;
import com.thenational.eu.testing.stubs.service.NameValuePairs;
import com.thenational.eu.testing.stubs.service.SequentialMockService;

@SequentialMockService(
        schema = "a",
        file = "seq-template",
        values = {
                         @NameValuePairs({
                                                 @NameValuePair(name = "sortCode", value = "840000"),
                                                 @NameValuePair(name = "accountNumber", value = "11111111"),
                                                 @NameValuePair(name = "customerNumber", value = "12345678")
                         }),
                         @NameValuePairs({
                                                 @NameValuePair(name = "sortCode", value = "840000"),
                                                 @NameValuePair(name = "accountNumber", value = "22222222"),
                                                 @NameValuePair(name = "customerNumber", value = "87654321")
                         })

        }
)
public class MockSomethingService {

}
