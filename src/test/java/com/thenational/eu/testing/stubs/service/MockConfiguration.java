package com.thenational.eu.testing.stubs.service;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static org.mockito.Mockito.mock;

@Configuration
public class MockConfiguration {

    @Bean
    public ResponseContentResolver mockResponseContextResolver() {
        return mock(ResponseContentResolver.class);
    }

    @Bean
    public Utils utils() {
        return mock(Utils.class);
    }

    @Bean
    public XPathMockServiceResultGenerator xPathMockServiceResultGenerator() {
        return mock(XPathMockServiceResultGenerator.class);
    }

    @Bean
    public MonotonicMockServiceResultGenerator monotonicMockServiceResultGenerator() {
        return mock(MonotonicMockServiceResultGenerator.class);
    }

    @Bean
    public SequentialMockServiceResultGenerator sequentialMockServiceResultGenerator() {
        return mock(SequentialMockServiceResultGenerator.class);
    }

    @Bean
    public RegexMockServiceResultGenerator regexMockServiceResultGenerator() {
        return mock(RegexMockServiceResultGenerator.class);
    }
}
