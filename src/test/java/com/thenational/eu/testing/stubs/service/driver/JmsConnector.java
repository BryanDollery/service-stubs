package com.thenational.eu.testing.stubs.service.driver;


import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.BrokerService;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Session;


public class JmsConnector {

    private Connection connection;
    private Session session;
    private BrokerService broker;
    private boolean createBroker;

    public JmsConnector(boolean createBroker) {
        this.createBroker = createBroker;
    }

    public Session getSession() {
        return session;
    }

    public void startConnection(ConnectionFactory connectionFactory, String connectionUrl) throws Exception {

        //Create Broker
        if (createBroker) {
            broker = new BrokerService();
            broker.setUseJmx(false);
            broker.setPersistent(false);
            broker.addConnector(connectionUrl);
            broker.start();

            // Create a ConnectionFactory
            connectionFactory = new ActiveMQConnectionFactory(connectionUrl);

        }

        // Create a Connection
        connection = connectionFactory.createConnection();
        connection.start();

        // Create a Session
        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
    }

    public void stopConnection() throws Exception {

        if (session != null) {
            session.close();
        }
        if (connection != null) {
            connection.stop();
        }
        if (createBroker && broker != null) {
            broker.stop();
        }
    }


}
