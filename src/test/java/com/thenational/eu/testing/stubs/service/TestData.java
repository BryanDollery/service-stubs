package com.thenational.eu.testing.stubs.service;

import java.lang.annotation.Annotation;

import static org.mockito.Mockito.mock;

public class TestData {
    public static final String XPATH_TEST_SCHEMA = "xpathTestSchema";
    public static final String MONOTONIC_TEST_SCHEMA = "monotonicTestSchema";
    public static final String SIMPLE_MONOTONIC_TEST_FILENAME = "test file";
    public static final String REGEX_TEST_SCHEMA = "wibbled";
    public static final String SEQUENTIAL_TEST_SCHEMA = "wibbleC";

    @MonotonicMockService(schema = MONOTONIC_TEST_SCHEMA, name = "wobble", file = "filename", path = "pathname", filePrefix = "pre", filePostfix = "post", fileExtension = ".wtf")
    public static class MonotonicTestService {
        static Annotation getAnnotation() {
            return MonotonicTestService.class.getAnnotation(MonotonicMockService.class);
        }
    }

    @MonotonicMockService(schema = MONOTONIC_TEST_SCHEMA, file = SIMPLE_MONOTONIC_TEST_FILENAME)
    public static class MonotonicTestServiceWithResolver {

        private ResponseContentResolver responseContentResolver;

        static Annotation getAnnotation() {
            return MonotonicTestServiceWithResolver.class.getAnnotation(MonotonicMockService.class);
        }

        public ResponseContentResolver getResponseContentResolver() {
            if (responseContentResolver == null)
                responseContentResolver = mock(ResponseContentResolver.class);

            return responseContentResolver;
        }
    }

    @XPathMockService(schema = XPATH_TEST_SCHEMA, xpath = "/a/b/c/text()", file = "defaultfile")
    public static class XpathTestService {

        private ResponseContentResolver responseContentResolver;

        static Annotation getAnnotation() {
            return XpathTestService.class.getAnnotation(XPathMockService.class);
        }

        public ResponseContentResolver getResponseContentResolver() {
            if (responseContentResolver == null)
                responseContentResolver = mock(ResponseContentResolver.class);

            return responseContentResolver;
        }
    }

    @XPathMockService(schema = XPATH_TEST_SCHEMA,
            file = "accountDetailsService",
            fileExtension = "hbm",
            namedXpaths = {
                                  @NameValuePair(name = "/event/account/id/text()", value = "accountId"),
                                  @NameValuePair(name = "/event/customer/id/text()", value = "customerId"),
            }
    )
    public static class XpathTemplatedTestService {

        private ResponseContentResolver responseContentResolver;

        static Annotation getAnnotation() {
            return XpathTemplatedTestService.class.getAnnotation(XPathMockService.class);
        }

        public ResponseContentResolver getResponseContentResolver() {
            if (responseContentResolver == null)
                responseContentResolver = mock(ResponseContentResolver.class);

            return responseContentResolver;
        }
    }

    @SequentialMockService(schema = SEQUENTIAL_TEST_SCHEMA)
    public static class SequentialTestService {
        static Annotation getAnnotation() {
            return SequentialTestService.class.getAnnotation(SequentialMockService.class);
        }
    }

    @RegexMockService(schema = REGEX_TEST_SCHEMA, regex = "")
    public static class RegexTestService {
        static Annotation getAnnotation() {
            return RegexTestService.class.getAnnotation(RegexMockService.class);
        }
    }


}