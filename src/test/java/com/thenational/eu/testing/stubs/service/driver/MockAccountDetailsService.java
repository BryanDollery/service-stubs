package com.thenational.eu.testing.stubs.service.driver;

import com.thenational.eu.testing.stubs.service.NameValuePair;
import com.thenational.eu.testing.stubs.service.TemplateValueHandler;
import com.thenational.eu.testing.stubs.service.XPathMockService;

@XPathMockService(schema = "http://schemas.eu.nabgroup.com/GCS_INQ_ACCTDETAILS_BS_V02/ACCTDETSINQRS/",
        file = "accountDetailsService",
        namedXpaths = {
                              @NameValuePair(name = "/account/id", value = "accountId"),
                              @NameValuePair(name = "/customer/id", value = "customerId"),
        }
)
public class MockAccountDetailsService {

    //<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><soap:Body><NagMsg xmlns:ns2="http://schemas.eu.nabgroup.com/GCS_INQ_ACCTDETAILS_BS_V02/ACCTDETSINQRS/" xmlns="http://schemas.eu.nabgroup.com/GCS_INQ_ACCTDETAILS_BS_V02/ACCTDETSINQRQ/"><GblMsgHdr><HdrInfo><HdrVersNbrId>001</HdrVersNbrId></HdrInfo><MsgResp><RespCode>0</RespCode><RsnCode>0</RsnCode><SnsDataDesc>0</SnsDataDesc></MsgResp><MsgIdn><SrcRoutInfo><Chnl>BPI</Chnl></SrcRoutInfo></MsgIdn><MsgAttrib><SrvcId>GCS_INQ_ACCTDETAILS_BS_V02</SrvcId><MsgType>Request</MsgType><RespReqdInd>Y</RespReqdInd><DtTm><LocalDtTm>2016-02-22T16:33:20.313Z</LocalDtTm></DtTm></MsgAttrib><PartyAttrib><OrigId>000000</OrigId><CnsmrId>ETCB0001</CnsmrId><CnsmrPrimFI>CB</CnsmrPrimFI></PartyAttrib></GblMsgHdr><SIGNONMSGSRQV1><SONRQ><CHANNELID>BPI</CHANNELID></SONRQ></SIGNONMSGSRQV1><PAYLOAD><PAYLOADTYPE>ACCTDETSINQRQ</PAYLOADTYPE><ACCTDETAILVIEW><BANKACCTINFO><BANKACCTINFOGRP TYPE="FROM"><BANKID>82</BANKID><BRANCHID>4707</BRANCHID><ACCTID>30089369</ACCTID><ACCTTYPE>IMP</ACCTTYPE></BANKACCTINFOGRP></BANKACCTINFO></ACCTDETAILVIEW></PAYLOAD></NagMsg></soap:Body></soap:Envelope>
    public boolean when(String in) {
        return true;
    }

    @TemplateValueHandler
    public String balance(String accountId) {
        if ("1".equals(accountId))
            return "200.00";
        else if ("2".equals(accountId))
            return "100.00";

        return "0.00";
    }


    public String response() {
        return "";
    }
}
