package com.thenational.eu.testing.stubs.service;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ServiceConfiguration.class})
public class TestStubService {
    @Autowired
    private StubService stubService;

    @Test
    @Ignore
    public void soap() throws Throwable {
        String response = stubService.esbReceiver("<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><NagMsg xmlns:ns2=\"http://schemas.eu.nabgroup.com/GCS_INQ_ACCTDETAILS_BS_V02/ACCTDETSINQRS/\" xmlns=\"http://schemas.eu.nabgroup.com/GCS_INQ_ACCTDETAILS_BS_V02/ACCTDETSINQRQ/\"><GblMsgHdr><HdrInfo><HdrVersNbrId>001</HdrVersNbrId></HdrInfo><MsgResp><RespCode>0</RespCode><RsnCode>0</RsnCode><SnsDataDesc>0</SnsDataDesc></MsgResp><MsgIdn><SrcRoutInfo><Chnl>BPI</Chnl></SrcRoutInfo></MsgIdn><MsgAttrib><SrvcId>GCS_INQ_ACCTDETAILS_BS_V02</SrvcId><MsgType>Request</MsgType><RespReqdInd>Y</RespReqdInd><DtTm><LocalDtTm>2016-02-22T16:33:20.313Z</LocalDtTm></DtTm></MsgAttrib><PartyAttrib><OrigId>000000</OrigId><CnsmrId>ETCB0001</CnsmrId><CnsmrPrimFI>CB</CnsmrPrimFI></PartyAttrib></GblMsgHdr><SIGNONMSGSRQV1><SONRQ><CHANNELID>BPI</CHANNELID></SONRQ></SIGNONMSGSRQV1><PAYLOAD><PAYLOADTYPE>ACCTDETSINQRQ</PAYLOADTYPE><ACCTDETAILVIEW><BANKACCTINFO><BANKACCTINFOGRP TYPE=\"FROM\"><BANKID>82</BANKID><BRANCHID>4707</BRANCHID><ACCTID>30089369</ACCTID><ACCTTYPE>IMP</ACCTTYPE></BANKACCTINFOGRP></BANKACCTINFO></ACCTDETAILVIEW></PAYLOAD></NagMsg></soap:Body></soap:Envelope>");
    }

    @Test
    @Ignore
    public void sequentialTemplates() throws Throwable {
        String response = stubService.esbReceiver("<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><NagMsg xmlns:ns2=\"a\" xmlns=\"a\"><GblMsgHdr><HdrInfo><HdrVersNbrId>001</HdrVersNbrId></HdrInfo><MsgResp><RespCode>0</RespCode><RsnCode>0</RsnCode><SnsDataDesc>0</SnsDataDesc></MsgResp><MsgIdn><SrcRoutInfo><Chnl>BPI</Chnl></SrcRoutInfo></MsgIdn><MsgAttrib><SrvcId>GCS_INQ_ACCTDETAILS_BS_V02</SrvcId><MsgType>Request</MsgType><RespReqdInd>Y</RespReqdInd><DtTm><LocalDtTm>2016-02-22T16:33:20.313Z</LocalDtTm></DtTm></MsgAttrib><PartyAttrib><OrigId>000000</OrigId><CnsmrId>ETCB0001</CnsmrId><CnsmrPrimFI>CB</CnsmrPrimFI></PartyAttrib></GblMsgHdr><SIGNONMSGSRQV1><SONRQ><CHANNELID>BPI</CHANNELID></SONRQ></SIGNONMSGSRQV1><PAYLOAD><PAYLOADTYPE>ACCTDETSINQRQ</PAYLOADTYPE><ACCTDETAILVIEW><BANKACCTINFO><BANKACCTINFOGRP TYPE=\"FROM\"><BANKID>82</BANKID><BRANCHID>4707</BRANCHID><ACCTID>30089369</ACCTID><ACCTTYPE>IMP</ACCTTYPE></BANKACCTINFOGRP></BANKACCTINFO></ACCTDETAILVIEW></PAYLOAD></NagMsg></soap:Body></soap:Envelope>");
    }
}
