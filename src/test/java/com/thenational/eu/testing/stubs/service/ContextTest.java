package com.thenational.eu.testing.stubs.service;

import com.thenational.eu.testing.stubs.service.TestData.SequentialTestService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class ContextTest {
    @Mock
    private Utils utils;

    @Test
    public void getName() {
        // given
        Context ctx = new Context(SequentialTestService.getAnnotation(), new SequentialTestService(), utils);

        // when
        String actual = ctx.getName();

        // then
        assertEquals("sequentialTestService", actual);
    }
}