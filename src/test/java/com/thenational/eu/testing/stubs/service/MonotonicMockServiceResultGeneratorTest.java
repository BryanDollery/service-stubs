package com.thenational.eu.testing.stubs.service;

import com.thenational.eu.testing.stubs.service.TestData.MonotonicTestServiceWithResolver;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static com.thenational.eu.testing.stubs.service.TestData.SIMPLE_MONOTONIC_TEST_FILENAME;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = MockConfiguration.class)
public class MonotonicMockServiceResultGeneratorTest {
    @Autowired
    private Utils utils;

    private MonotonicMockServiceResultGenerator generator = new MonotonicMockServiceResultGenerator();

    @Test
    public void generate() {
        // given
        MonotonicTestServiceWithResolver bean = new MonotonicTestServiceWithResolver();
        ResponseContentResolver responseContentResolver = bean.getResponseContentResolver();
        doReturn(responseContentResolver).when(utils).findResponseContentResolver(bean);

        // when
        String actual = generator.generate("ignored", new Context(MonotonicTestServiceWithResolver.getAnnotation(), bean, utils));

        // then
        verify(bean.getResponseContentResolver()).resolve(SIMPLE_MONOTONIC_TEST_FILENAME);
    }

}
