package com.thenational.eu.testing.stubs.service;

import com.thenational.eu.testing.stubs.service.TestData.XpathTemplatedTestService;
import com.thenational.eu.testing.stubs.service.TestData.XpathTestService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = MockConfiguration.class)
public class XPathMockServiceResultGeneratorTest {

    public static final String EXPECTED = "expected";
    public static final String FOUND = "found";
    public static final String CUSTOMER_AND_ACCOUNT_REQUEST = "<event><account><id>accntid</id></account><customer><id>cstmrid</id></customer></event>";
    public static final String CUSTOMER_AND_ACCOUNT_TEMPLATE = "Found {{accountId}} and {{customerId}}";
    public static final String CUSTOMER_AND_ACCOUNT_RESPONSE = "Found accntid and cstmrid";
    public static final String DEFAULT_FILE = "defaultfile.xml";

    @Autowired
    private Utils utils;

    private XPathMockServiceResultGenerator generator = new XPathMockServiceResultGenerator();

    @Test
    public void useXpathToFindFilename() {
        // given
        XpathTestService service = new XpathTestService();
        Context ctx = new Context(XpathTestService.getAnnotation(), service, utils);
        ResponseContentResolver responseContentResolver = service.getResponseContentResolver();
        doReturn(EXPECTED).when(responseContentResolver).resolve(FOUND + ".xml"); // the filename comes from the xml message and the default extension (.xml)
        doReturn(responseContentResolver).when(utils).findResponseContentResolver(service);

        // when
        String actual = generator.generate("<a><b><c>" + FOUND + "</c></b></a>", ctx); // the xpath is /a/b/c/text()

        // then
        assertEquals("expected", actual);
    }

    @Test
    public void useDefaultFilename() {
        // given
        XpathTestService service = new XpathTestService();
        Context ctx = new Context(XpathTestService.getAnnotation(), service, utils);
        ResponseContentResolver responseContentResolver = service.getResponseContentResolver();
        doReturn(EXPECTED).when(responseContentResolver).resolve(DEFAULT_FILE);
        doReturn(responseContentResolver).when(utils).findResponseContentResolver(service);

        // when
        String actual = generator.generate("<x><y><z>" + FOUND + "</z></y></x>", ctx); // the xpath is /a/b/c/text()

        // then
        assertEquals("expected", actual);
    }

    @Test
    public void template() {
        // given
        XpathTemplatedTestService service = new XpathTemplatedTestService();
        Context ctx = new Context(XpathTemplatedTestService.getAnnotation(), service, utils);
        ResponseContentResolver responseContentResolver = service.getResponseContentResolver();
        doReturn(CUSTOMER_AND_ACCOUNT_TEMPLATE).when(responseContentResolver).resolve(DEFAULT_FILE); // the filename comes from the xml message and the default extension (.xml)
        doReturn(responseContentResolver).when(utils).findResponseContentResolver(service);

        // when
        String actual = generator.generate(CUSTOMER_AND_ACCOUNT_REQUEST, ctx);

        // then
        assertEquals(CUSTOMER_AND_ACCOUNT_RESPONSE, actual);
    }
}
