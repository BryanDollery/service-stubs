package com.thenational.eu.testing.stubs.service;

import com.thenational.eu.testing.stubs.service.TestData.MonotonicTestService;
import com.thenational.eu.testing.stubs.service.TestData.MonotonicTestServiceWithResolver;
import com.thenational.eu.testing.stubs.service.TestData.RegexTestService;
import com.thenational.eu.testing.stubs.service.TestData.SequentialTestService;
import com.thenational.eu.testing.stubs.service.TestData.XpathTestService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.ApplicationContext;

import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;

@RunWith(MockitoJUnitRunner.class)
public class UtilsTest {
    public static final String XPATH_TEST_SCHEMA = "xpathTestSchema";
    public static final String MONOTONIC_TEST_SCHEMA = "monotonicTestSchema";

    @Mock
    private XPathMockServiceResultGenerator xPathMockServiceResultGenerator;

    @Mock
    private MonotonicMockServiceResultGenerator monotonicMockServiceResultGenerator;

    @Mock
    private SequentialMockServiceResultGenerator sequentialMockServiceResultGenerator;

    @Mock
    private RegexMockServiceResultGenerator regexMockServiceResultGenerator;

    @Mock
    private ResponseContentResolver responseContentResolver;

    @Mock
    private ApplicationContext applicationContext;

    @InjectMocks
    private Utils utils;

    private Annotation annotation = MonotonicTestService.getAnnotation();

    private Context ctx = new Context(annotation, new MonotonicTestService(), utils);

    @Test
    public void defaultContentResolver() {
        // given

        // when
        ResponseContentResolver actual = utils.findResponseContentResolver(ctx.getBean());

        // then
        assertEquals(responseContentResolver, actual);
    }

    @Test
    public void overrideContentResolver() {
        // given
        MonotonicTestServiceWithResolver service = new MonotonicTestServiceWithResolver();

        // when
        ResponseContentResolver actual = utils.findResponseContentResolver(service);

        // then
        assertEquals(service.getResponseContentResolver(), actual);
    }

    @Test
    public void getNameFromAnnotation() {
        assertEquals("wobble", Utils.getName(MonotonicTestService.getAnnotation()));
    }

    @Test
    public void getSchemaFromAnnotation() {
        assertEquals(MONOTONIC_TEST_SCHEMA, Utils.getSchema(MonotonicTestService.getAnnotation()));
    }

    @Test
    public void getNameFromContext() {
        // when
        String actual = Utils.getName(ctx);

        // then
        assertEquals("wobble", actual);
    }

    @Test
    public void getSchemaFromContext() {
        // when
        String actual = Utils.getSchema(ctx);

        // then
        assertEquals(MONOTONIC_TEST_SCHEMA, actual);
    }

    @Test
    public void getFile() {
        // when
        String actual = Utils.getFile(ctx);

        // then
        assertEquals("filename", actual);
    }

    @Test
    public void getPath() {
        // when
        String actual = Utils.getPath(ctx);

        // then
        assertEquals("pathname", actual);
    }

    @Test
    public void getPrefix() {
        // when
        String actual = Utils.getPrefix(ctx);

        // then
        assertEquals("pre", actual);
    }

    @Test
    public void getPostfix() {
        // given

        // when
        String actual = Utils.getPostfix(ctx);

        // then
        assertEquals("post", actual);
    }

    @Test
    public void getExtension() {
        // given

        // when
        String actual = Utils.getExtension(ctx);

        // then
        assertEquals(".wtf", actual);
    }

    @Test
    public void findContextForSchema() {
        // given
        Map<String, Object> appCtx = new HashMap<>();
        appCtx.put("monotonicTestClass", new MonotonicTestService());
        appCtx.put("dummyClassTwo", new XpathTestService());
        appCtx.put("dummyClassThree", new SequentialTestService());
        appCtx.put("dummyClassFour", new RegexTestService());
        doReturn(appCtx).when(applicationContext).getBeansWithAnnotation(MockService.class);

        // when
        Context context = utils.findContext(XPATH_TEST_SCHEMA);

        // then
        assertEquals(XPATH_TEST_SCHEMA, context.getSchema());
        assertEquals(XPathMockService.class, context.getAnnotation().annotationType());
        assertEquals(XpathTestService.class, context.getBean().getClass());
    }

    @Test
    public void findGenerator() {
        // given
        Map<String, Object> appCtx = new HashMap<>();
        appCtx.put("monotonicTestClass", new MonotonicTestService());
        appCtx.put("dummyClassTwo", new XpathTestService());
        appCtx.put("dummyClassThree", new SequentialTestService());
        appCtx.put("dummyClassFour", new RegexTestService());
        doReturn(appCtx).when(applicationContext).getBeansWithAnnotation(MockService.class);
        Context context = utils.findContext(XPATH_TEST_SCHEMA);

        // when
        MockServiceResultGenerator generator = utils.findGenerator(context);

        // then
        assertEquals(xPathMockServiceResultGenerator.getClass(), generator.getClass()); // compare mocks
    }


}