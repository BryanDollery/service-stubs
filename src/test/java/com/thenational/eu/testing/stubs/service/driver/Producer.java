package com.thenational.eu.testing.stubs.service.driver;


import javax.jms.*;
import java.util.ArrayList;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

public class Producer {

    private List<Message> producedMessages;

    private MessageProducer producer;
    private Session session;

    public List<Message> getProducedMessages() {
        return producedMessages;
    }

    public Producer(Session session, Destination destination) throws Exception {

        checkNotNull(session,"Session should be initialised");
        checkNotNull(destination,"Destination should be initialised");

        this.session = session;
        producer = session.createProducer(destination);
        producedMessages = new ArrayList<>();

    }

    public void sendTextMessage(String txt, Destination jmsReplyTo) throws Exception {
        TextMessage message = session.createTextMessage();
        message.setText(txt);
        if( jmsReplyTo != null ){
            message.setJMSReplyTo(jmsReplyTo);
        }
        producer.send(message);
        producedMessages.add(message);
        System.out.println("Sent  message '" + message.getText() + "'");
    }
}
