package com.thenational.eu.testing.stubs.service;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(ServiceConfiguration.class)
public class FileResolverConfiguration {
    @Bean
    public ResponseContentResolver responseContentResolver() {
        return new FileResponseContentResolver();
    }
}
