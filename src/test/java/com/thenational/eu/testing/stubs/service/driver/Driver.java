package com.thenational.eu.testing.stubs.service.driver;

import com.google.common.io.Resources;
import com.thenational.eu.testing.stubs.service.FileResolverConfiguration;
import com.thenational.eu.testing.stubs.service.ServiceConfiguration;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.Session;

import static com.google.common.base.Charsets.UTF_8;
import static com.google.common.io.Resources.getResource;
import static com.thenational.eu.testing.stubs.service.ServiceConfiguration.DESTINATION_URL;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(ServiceConfiguration.class)
@ContextConfiguration(classes = {FileResolverConfiguration.class})
public class Driver {
    public static final String SCHEDULE_SWEEP_TOPIC = "schedule/sweep/#";
    private JmsConnector jmsConnector;
    private Producer producer;

    @Before
    public void setup() throws Exception {
        final ConnectionFactory sourceConnectionFactory = new ActiveMQConnectionFactory(DESTINATION_URL);
        jmsConnector = new JmsConnector(false);
        jmsConnector.startConnection(sourceConnectionFactory, DESTINATION_URL);

        Session session = jmsConnector.getSession();
        final Destination inputTopic = session.createTopic(SCHEDULE_SWEEP_TOPIC);
        producer = new Producer(session, inputTopic);
    }

    @Test
    @Ignore
    public void go() throws Exception {
        Driver driver = new Driver();

        driver.setup();
        driver.fire();
        driver.tearDown();
    }

    @After
    public void tearDown() throws Exception {
        if (jmsConnector != null) {
            jmsConnector.stopConnection();
        }
    }

    private void fire() throws Exception {
        final String message = retrieveFileContents("ScheduledTrigger.xml");
        System.out.println("Sending message...");
        producer.sendTextMessage(message, null);
    }

    private String retrieveFileContents(final String path) throws Exception {
        return Resources.toString(getResource(path), UTF_8);
    }

}
